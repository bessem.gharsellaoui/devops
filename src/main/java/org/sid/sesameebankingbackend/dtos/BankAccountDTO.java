package org.sid.sesameebankingbackend.dtos;

import lombok.Data;

@Data
public class BankAccountDTO {
    private String type;
}
