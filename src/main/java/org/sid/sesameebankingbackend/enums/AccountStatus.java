package org.sid.sesameebankingbackend.enums;

public enum AccountStatus {
    CREATED, ACTIVATED, SUSPENDED
}
