package org.sid.sesameebankingbackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.sid.sesameebankingbackend.dtos.CustomerDTO;
import org.sid.sesameebankingbackend.repositories.CustomerRepository;
import org.sid.sesameebankingbackend.services.BankAccountService;
import org.sid.sesameebankingbackend.services.BankAccountServiceImpl;
import org.sid.sesameebankingbackend.web.CustomerRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.annotation.PostConstruct;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;

@WebMvcTest(CustomerRestController.class)
class CustomerRestControllerTest {

    @TestConfiguration
    static class TestConfig {

        @Bean
        CommandLineRunner commandLineRunner2(BankAccountService bankAccountService) {
            return args -> {
                Stream.of("Bassem", "Jesser", "Mohamed").forEach(name -> {
                    CustomerDTO customer = new CustomerDTO();
                    customer.setName(name);
                    customer.setEmail(name + "@gmail.com");
                    bankAccountService.saveCustomer(customer);
                });
            };

        }
    }

        @Autowired
        MockMvc mockMvc;
        @Autowired
        ObjectMapper mapper;

        @MockBean
        private BankAccountServiceImpl bankAccountServiceImpl;


        @Test
        public void getAllRecords_success() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders
                            .get("/customers")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(3)))
                    .andExpect(jsonPath("$[1].name", is("Bassem")));
        }



}
